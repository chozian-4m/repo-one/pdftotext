# Pdftotext

pdftotext is an open-source command-line utility for converting PDF files to plain text files—i.e. extracting text data from PDF-encapsulated files. It is freely available and included by default with many Linux distributions, and is also available for Windows as part of the Xpdf Windows port. Such text extraction is complicated as PDF files are internally built on page drawing primitives, meaning the boundaries between words and paragraphs often must be inferred based on their position on the page.

pdftotext is part of the Xpdf software suite. Poppler, which is derived from Xpdf, also includes an implementation of pdftotext. On most Linux distributions, pdftotext is included as part of the poppler-utils package

The pdftotext software and documentation are copyright 1996-2021 Glyph & Cog, LLC.

# Documentation

For more information please refer to https://www.xpdfreader.com/pdftotext-man.html


