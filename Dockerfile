ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5


FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}


COPY xpdf.tar.gz xpdf-tools-linux.tar.gz poppler-utils.rpm /
RUN tar -xzf xpdf.tar.gz  
RUN tar -xzf xpdf-tools-linux.tar.gz  && \
    rm -rf xpdf-tools-linux.tar.gz xpdf.tar.gz && \
    cp xpdf-tools-linux-4.03/bin64/pdftotext /usr/bin/pdftotext



USER 1001

ENTRYPOINT ["/usr/bin/pdftotext"]

CMD ["-layout", "-", "-"] 
HEALTHCHECK NONE

